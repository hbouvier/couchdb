package couchdb

// version is set at build time using the go `-ldflags` like this:
// 		-ldflags "-X main.version=0.0.0"
var version = "0.0.1"
