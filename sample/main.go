package main

import (
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/hbouvier/couchdb"
)

var version = "snapshot"

func main() {
	log.Println("Sample version: v" + version)
	log.Println("GO CouchDB API version: v" + couchdb.Version())

	signalsChanel := make(chan os.Signal, 1)
	signal.Notify(signalsChanel, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGQUIT)
	timeToTerminate := make(chan struct{})
	var canTerminateSafely sync.WaitGroup // canTerminateSafely.Add(1) in each go routine
	go work(&canTerminateSafely, timeToTerminate, signalsChanel)

	signals := map[os.Signal]int{
		syscall.SIGHUP:  1,
		syscall.SIGINT:  2,
		syscall.SIGQUIT: 3,
		syscall.SIGTERM: 15,
	}
	sig := <-signalsChanel
	var code = 0
	if sig != nil {
		code = 128 + signals[sig]
		log.Printf("Shutdown signal (" + strconv.Itoa(signals[sig]) + ") received, exiting...")
		close(timeToTerminate)
	}
	canTerminateSafely.Wait()
	os.Exit(code)
}

func work(canTerminateSafely *sync.WaitGroup, timeToTerminate chan struct{}, signalsChanel chan os.Signal) {
	canTerminateSafely.Add(1)
	defer func() {
		canTerminateSafely.Done()
		close(signalsChanel)
	}()

	server := couchdb.Connect("http://127.0.0.1:5984")
	for {
		err := server.Ready()
		if err == nil {
			break
		}
		log.Println(err)
		select {
		case <-time.After(time.Second): // wait one second
			continue
		case <-timeToTerminate: // got
			return
		}
	}

	// namespaces, err := server.GetNamespaces()
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }

	// namespaces.Items.Each(func(namespace kubernetes.Namespace) {
	// 	log.Println(namespace.Metadata.Name)
	// })
}
