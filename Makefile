GO=go
VERSION=v$(shell cat VERSION)
INSTALL_FLAGS=-ldflags "-X main.version=${VERSION}"

all:	generate fmt run

clean:
	@rm -rf fungen_auto.go

# generate:	fungen_auto.go
generate:

# fungen_auto.go: namespace.go server.go
# 	${GO} generate

fmt:
	${GO} fmt

run:
	${GO} run sample/main.go

