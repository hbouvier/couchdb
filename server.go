package couchdb

// _go_ :_ generate fungen -package kubernetes -types string,ServerAddressByClientCIDR,Namespace

import (
	"fmt"

	http "gitlab.com/hbouvier/http"
)

type Couchdb struct {
	client *http.Client
}

type up struct {
	Status string `json:"status"`
}

func Version() string {
	return version
}
func Connect(url string) *Couchdb {
	client := http.New(url, nil, map[string]string{"Content-Type": "application/json; charset=UTF8"})
	return &Couchdb{client: &client}
}

func (server *Couchdb) Ready() error {
	var payload up
	_, err := server.client.Get("/_up", nil, &payload)
	if err != nil {
		return err
	}
	if payload.Status != "ok" {
		return fmt.Errorf(payload.Status)
	}
	return nil
}
